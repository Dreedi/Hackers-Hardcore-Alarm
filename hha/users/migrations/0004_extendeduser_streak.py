# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20151004_0150'),
    ]

    operations = [
        migrations.AddField(
            model_name='extendeduser',
            name='streak',
            field=models.IntegerField(default=0),
        ),
    ]
