import json
from datetime import datetime

from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse
from django.contrib.auth.models import User

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .models import ExtendedUser, Date
from .serializers import UserSerializer, DateSerializer

class HelloView(View):
	def get(self, request):
		return HttpResponse("Hello")

class SignUpUserView(APIView):
	def post(self, request):
		received_data = json.loads(request.body)
		serializer = UserSerializer(data=received_data)
		response_data = {
			'correct': False
		}
		if serializer.is_valid():
			new_user = User.objects.create_user(
				serializer.data['username'],
				'',
				serializer.data['password'],
			)
			response_data['correct'] = True
			response_data.update({
				'username': serializer.data['username'],
				'id': new_user.pk,
			})

			new_extended_user = ExtendedUser(
				user=new_user,
			)
			new_extended_user.save()

		return Response(response_data)

class AddDateView(APIView):
	def post(self, request):
		received_data = json.loads(request.body)
		date_dict = {
			'year': received_data.get('year', ''),
			'month': received_data.get('month', ''),
			'day': received_data.get('day', ''),
			'hour': received_data.get('hour', ''),
			'minute': received_data.get('minute', ''),
		}
		date = ()
		try:
			date = datetime(
				received_data.get('year', ''),
				received_data.get('month', ''),
				received_data.get('day', ''),
				received_data.get('hour', ''),
				received_data.get('minute', ''),		
			)
			received_data.update({
				'date': str(date),
			})
		except:
			pass
		extended_user = ()
		try:
			extended_user = ExtendedUser.objects.get(user__pk=received_data.get('user', ''))
			received_data.update({
				'user': extended_user.pk,
			})
		except:
			pass
		serializer = DateSerializer(data=received_data)
		response_data = {
			'correct': False
		}

		if serializer.is_valid():
			new_date = Date(
				date=date,
				good=serializer.validated_data['good'],
				user=serializer.validated_data['user']
			)
			new_date.save()
			response_data['correct'] = True
			if serializer.validated_data['good']:
				serializer.validated_data['user'].streak += 1
				serializer.validated_data['user'].score += (1 * int((serializer.validated_data['user'].streak / 2 + 1)))
			else:
				serializer.validated_data['user'].streak = 0
				if serializer.validated_data['user'].score <= 5:
					serializer.validated_data['user'].score = 0
				else:
					serializer.validated_data['user'].score -= 5
			serializer.validated_data['user'].save()
		else:
			print serializer.errors

		#serializer = DateSerializer(data=received_data)

		return Response(response_data)

class GetDatesView(APIView):
	def get(self, request, user_id):
		try:
			extended_user = ExtendedUser.objects.get(user__pk=user_id)
		except:
			extended_user = ()
		dates = []
		for date in Date.objects.filter(user=extended_user):
			dates.append({
				'year': date.date.year,
				'month': date.date.month,
				'day': date.date.day,
				'hour': date.date.hour,
				'minute': date.date.minute,
				'good': date.good,
			})
		return Response(dates)

class GetScoreView(APIView):
	def get(self, request, user_id):
		return_dict = {}
		try:
			extended_user = ExtendedUser.objects.get(user__pk=user_id)
			return_dict = {
				'score': extended_user.score,
				'streak': extended_user.streak,
			}
		except:
			extended_user = ()
		return Response(return_dict)