from django.conf.urls import include, url

from . import views

urlpatterns = [
    url(
    	r'^$',
    	views.HelloView.as_view(),
    	name='hello'
    ),
    url(
    	r'^signup',
    	views.SignUpUserView.as_view(),
    	name='sign_up'
    ),
    url(
    	r'^add-date',
    	views.AddDateView.as_view(),
    	name='add_date'
    ),
    url(
    	r'^(?P<user_id>\d+)/dates',
    	views.GetDatesView.as_view(),
    	name='get_dates'
    ),
    url(
    	r'^(?P<user_id>\d+)/score',
    	views.GetScoreView.as_view(),
    	name='get_score'
    ),
]
