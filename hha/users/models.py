from django.contrib.auth.models import User
from django.db import models

class ExtendedUser(models.Model):
	user = models.OneToOneField(User)
	score = models.IntegerField(default=0)
	streak = models.IntegerField(default=0)

	def __unicode__(self):
		return self.user.username

class Date(models.Model): 
	date = models.DateTimeField()
	good = models.BooleanField()
	user = models.ForeignKey(ExtendedUser)

	def __unicode__(self):
		return str(self.date)