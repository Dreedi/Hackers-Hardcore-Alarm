# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Date',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField()),
                ('good', models.BooleanField()),
            ],
        ),
        migrations.RemoveField(
            model_name='extendeduser_days',
            name='day',
        ),
        migrations.RemoveField(
            model_name='extendeduser_days',
            name='extended_user',
        ),
        migrations.RemoveField(
            model_name='extendeduser',
            name='days',
        ),
        migrations.DeleteModel(
            name='Day',
        ),
        migrations.DeleteModel(
            name='ExtendedUser_Days',
        ),
        migrations.AddField(
            model_name='date',
            name='user',
            field=models.ForeignKey(to='users.ExtendedUser'),
        ),
    ]
