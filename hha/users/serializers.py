from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Date

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'password']

class DateSerializer(serializers.ModelSerializer):
	class Meta:
		model = Date