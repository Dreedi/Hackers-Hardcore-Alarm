# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20151004_0136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='extendeduser',
            name='score',
            field=models.IntegerField(default=0),
        ),
    ]
