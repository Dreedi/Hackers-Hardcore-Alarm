from django.contrib import admin

from .models import (
	ExtendedUser,
	Date,
)

class ExtendedUserAdmin(admin.ModelAdmin):
	list_display = ['user', 'score']
	search_fields = ['score']
	list_editable = ['score']

class DateAdmin(admin.ModelAdmin):
	list_display = ['date', 'good']
	list_editable = ['good']

admin.site.register(ExtendedUser, ExtendedUserAdmin)
admin.site.register(Date, DateAdmin)